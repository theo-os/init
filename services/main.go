// Package services implements logic to read and parse service units
// and running services. Monitoring is bloat. Just kidding, it's too hard ftm
package services

import (
	"context"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path"
	"path/filepath"
	"runtime"
	"strings"
	"sync"

	"golang.org/x/sync/semaphore"
	yaml "gopkg.in/yaml.v3"
)

// Dir where services will be looked
var ServicesDir = "/etc/yml-services/"

// Errors.
var (
	ErrDuplicatedName = errors.New("duplicate service")
)

var (
	logger = log.New(os.Stderr, "services ", log.LstdFlags)
)

type ServiceManager struct {
	ctx                   context.Context
	services              []*Service
	mtx                   sync.Mutex
	mappedServices        map[string]*Service
	deadChildrenBroadcast []chan<- bool
}

// Create a new ServiceManager and read all service files
func NewServiceManager(ctx context.Context, incomingDeadChildren <-chan bool) (*ServiceManager, error) {
	services, mappedServices, err := readServices(ctx)
	if err != nil {
		return nil, err
	}
	sm := &ServiceManager{
		ctx:            ctx,
		services:       services,
		mappedServices: mappedServices,
	}
	sm.prepareChannels(incomingDeadChildren)
	return sm, nil
}

func readServices(ectx context.Context) (services []*Service, mappedServices map[string]*Service, err error) {
	ctx, cancel := context.WithCancel(ectx)
	defer cancel()

	fileList, err := ioutil.ReadDir(ServicesDir)
	if err != nil {
		err = fmt.Errorf("read services: %w", err)
		return
	}

	var (
		smp = semaphore.NewWeighted(int64(runtime.NumCPU()))
		wg  sync.WaitGroup
	)

	mappedServices = make(map[string]*Service)
	mtx := new(sync.Mutex)

	for _, file := range fileList {
		if file.IsDir() {
			continue
		}

		if name := file.Name(); !(strings.HasSuffix(name, ".yml") || strings.HasSuffix(name, ".yaml")) {
			continue
		}

		if err = smp.Acquire(ctx, 1); err != nil {
			return
		} else if err = ctx.Err(); err != nil {
			return
		}

		wg.Add(1)
		go func(name string) {
			pth := path.Join(ServicesDir, name)
			defer wg.Done()
			defer smp.Release(1)
			file, err := os.Open(pth)
			if err != nil {
				logger.Printf("[SKIP-PRE] Cannot open %s to load service: %s\n", pth, err)
				return
			}
			defer file.Close()

			svcs, err := parseService(file)
			if err != nil {
				logger.Printf("[SKIP] Error loading service %s: %s\n", pth, err)
				return
			}

			for _, svc := range svcs {
				if err = svc.Validate(); err != nil {
					logger.Printf("[SKIP-POST] Error validating service: %s: %s\n", pth, err)
					return
				}

				func() {
					mtx.Lock()
					defer mtx.Unlock()

					// Check for duplicate services
					if _, e := mappedServices[svc.Name]; e {
						logger.Printf("[SKIP] Duplicate service with name %s. Skipping duplicate services\n", svc.Name)
						return
					}

					services = append(services, svc)
					mappedServices[svc.Name] = svc
				}()
			}
		}(file.Name())
	}

	wg.Wait()

	return
}

func (sm *ServiceManager) prepareChannels(incomingDeadChildren <-chan bool) {
	for _, svc := range sm.services {
		dcc := make(chan bool, 2)
		svc.deadChildren = dcc
		sm.deadChildrenBroadcast = append(sm.deadChildrenBroadcast, dcc)
	}

	go sm.listenDeadChild(incomingDeadChildren)
}

func (sm *ServiceManager) listenDeadChild(channel <-chan bool) {
	for b := range channel {
		for _, ch := range sm.deadChildrenBroadcast {
			select {
			case ch <- b:
			default: // Don't block here
				logger.Printf("Channel on %p is losing messages\n", ch)
			}
		}
	}
}

func parseService(file *os.File) ([]*Service, error) {
	var (
		from        = io.Reader(file)
		_, argument = SplitArgument(filepath.Base(file.Name()))
		err         error
	)
	if argument != "" {
		if from, err = ApplyTemplate(file, file.Name(), argument); err != nil {
			return nil, err
		}
	}

	svcs := make([]*Service, 0, 1)
	decoder := yaml.NewDecoder(from)
	decoder.KnownFields(true)
	return svcs, decoder.Decode(&svcs)
}
