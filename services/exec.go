package services

import (
	"context"
	"errors"
	"init/journal"
	"io"
	"os"
	"runtime"
	"sync"
	"time"

	"golang.org/x/sync/semaphore"
)

func (sm *ServiceManager) StartAll(ctx context.Context) error {
	var (
		smp          = semaphore.NewWeighted(int64(runtime.NumCPU()))
		wg           sync.WaitGroup
		serviceNames = make(map[string]*Service)
	)

	wg.Add(1)
	go func() {
		defer wg.Done()
		for _, service := range sm.services {
			serviceNames[service.Name] = service
		}
	}()

	// First we start services w/o dependencies
	for _, service := range sm.services {
		if len(service.Dependencies) != 1 {
			continue
		}

		if err := smp.Acquire(ctx, 1); err != nil {
			return err
		}

		wg.Add(1)

		go func(service *Service) {
			defer smp.Release(1)
			defer wg.Done()

			err := service.Start(ctx, GenerateJournal(service))
			if err != nil {
				logger.Printf("Error starting service %s w/o dependencies: %s\n", service.Name, err)
			}
		}(service)
	}

	wg.Wait()

	// Create another one
	wg = sync.WaitGroup{}

	// Now we start processes w/ dependencies
	// We're not using the semaphore bc most goroutines will be blocked waiting and not doing actual work
	for _, service := range sm.services {
		// Service was already started
		if !errors.Is(service.State.Err(), ErrNotStarted) {
			continue
		}

		wg.Add(1)
		go func(service *Service) {
			defer wg.Done()

			deps, notFound, cyclic := getDependencies(serviceNames, service.Dependencies)
			service.foundDependencies = deps
			if cyclic {
				logger.Printf("Cyclic dependency found on service %s (depends on %v)\n", service.Name, service.Dependencies)
				service.State.ErrorBadConfig()
				return
			} else if len(notFound) != 0 {
				logger.Printf("Didn't find dependencies %v for service %s\n", notFound, service.Dependencies)
				service.State.ErrorBadConfig()
				return
			}

			if err := service.Start(ctx, GenerateJournal(service)); err != nil {
				logger.Printf("Error starting service %s: %s\n", service.Name, err)
			}
		}(service)
	}

	wg.Wait()

	return nil
}

func GenerateJournal(service *Service) *journal.Journal {
	var (
		stdout, stderr io.Writer
	)

	if service.ForwardJournal {
		stdout = os.Stdout
		stderr = os.Stderr
	}

	return journal.NewJournal(service.Name, stdout, stderr)
}

func (sm *ServiceManager) StopAll(ctx context.Context) {
	var wg sync.WaitGroup

	for _, service := range sm.services {
		if !service.IsProcessAlive() {
			continue
		}

		wg.Add(1)
		tctx, cancel := context.WithTimeout(ctx, time.Second*60)
		defer cancel()
		go func(ctx context.Context, service *Service) {
			defer wg.Done()
			logger.Printf("Stopping service %s\n", service.Name)
			if err := service.Stop(ctx); err != nil {
				logger.Printf("Error stopping service %s: %s\n", service.Name, err)
			}
		}(tctx, service)
	}

	wg.Wait()
}
