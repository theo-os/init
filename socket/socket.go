package socket

import (
	"context"
	"errors"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"strings"
)

// Errors.
var (
	ErrCannotParse = errors.New("cannot parse command")
)

// Default socket address
var SocketAddr = "/run/service-launcher.sock"

var logger = log.New(os.Stdout, "socket ", log.LstdFlags)

type CommandHandler func(command, args string, result io.WriteCloser)

type Listener struct {
	listener       net.Listener
	commandHandler map[string]CommandHandler
}

type Command struct {
	Command string
	Args    string
}

func NewListener(commandHandlers map[string]CommandHandler) (*Listener, error) {
	_ = os.Remove(SocketAddr) // Ensure there is no socket listening there

	listener, err := net.Listen("unix", SocketAddr)
	if err != nil {
		return nil, fmt.Errorf("cannot create unix pipe: %w", err)
	}

	if err = os.Chmod(SocketAddr, 0o600); err != nil {
		return nil, fmt.Errorf("cannot set mode on pipe: %w", err)
	}

	logger.Printf("Socket listening on %q\n", SocketAddr)

	return &Listener{
		listener:       listener,
		commandHandler: commandHandlers,
	}, nil
}

func (l *Listener) Listen(ctx context.Context) {
	for {
		conn, err := l.listener.Accept()
		if err != nil {
			if ctx.Err() != nil {
				return
			}

			logger.Printf("Error on Accept: %s\n", err)

			return
		}

		go l.handleConn(conn)
	}
}

func (l *Listener) handleConn(conn net.Conn) {
	defer func() {
		if err := recover(); err != nil {
			logger.Printf("Recovered panic from command handler: %s\n", err)
		}
	}()

	cmd, err := readFromPipe(conn)
	if err != nil {
		fmt.Fprintf(conn, "Error parsing command: %s\n", err)
		conn.Close()

		return
	}

	handler, exists := l.commandHandler[cmd.Command]
	if !exists {
		fmt.Fprintf(conn, "Cannot find command manager for command %q\n", cmd.Command)
		conn.Close()

		return
	}

	handler(cmd.Command, cmd.Args, conn)
}

func (l *Listener) Close() {
	logger.Println("Removing socket...")
	l.listener.Close()
	os.Remove(SocketAddr)
}

func readFromPipe(pipe io.Reader) (Command, error) {
	var buf [512]byte

	n, err := io.ReadAtLeast(pipe, buf[:], 1)
	if err != nil && !errors.Is(err, io.EOF) {
		return Command{}, fmt.Errorf("error reading from conn: %w", err)
	}

	spl := strings.SplitN(strings.TrimSuffix(string(buf[:n]), "\n"), " ", 2)
	if len(spl) < 1 || spl[0] == "" {
		return Command{}, fmt.Errorf("%w: %q", ErrCannotParse, spl)
	}

	var args string
	if len(spl) > 1 {
		args = spl[1]
	}

	return Command{
		Command: spl[0],
		Args:    args,
	}, nil
}
