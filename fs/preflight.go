package fs

import (
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"
)

//Errors.
var (
	ErrBadMode = errors.New("bad file mode")
)

var preflightHooks = map[string]PreflightHook{
	"x-mount.mkdir": hookMkdir,
	"X-mount.mkdir": hookMkdir,
}

// Hooks execute other syscalls before `mount(2)` is called.
// Those hooks may fail (err!=nil) and will stop the flow.
// Option will be the option that triggered this hook and spec the current filesystem
// that includes that option on it's `Data` field.
type PreflightHook func(option string, spec FilesystemSpec) error

func hookMkdir(option string, spec FilesystemSpec) (err error) {
	var mode = os.FileMode(0o711) // The most restrictive. Mounted filesystems often come with their own mode
	spl := strings.SplitN(option, "=", 2)
	switch len(spl) {
	case 1:
		// Nothing to do, stick with the default
	case 2:
		if mode, err = parseMode(spl[1]); err != nil {
			return
		}
	default:
		err = fmt.Errorf("%w for mount %s: %s", ErrBadMode, spec.Dest, err)
	}

	return os.MkdirAll(spec.Dest, mode)
}

func parseMode(modeStr string) (mode os.FileMode, err error) {
	var num uint64
	if num, err = strconv.ParseUint(modeStr, 8, 32); err != nil {
		return 0, err
	}

	mode = os.FileMode(num)
	return
}

// Extract preflight hooks from options. Returns a map with the option that triggered the hook as key
func (m FilesystemSpec) Preflight() map[string]PreflightHook {
	output := make(map[string]PreflightHook)
	spl := strings.Split(m.Data, ",")

	for _, flag := range spl {
		if hook := isPreflight(flag); hook != nil {
			output[flag] = hook
		}
	}

	return output
}

func isPreflight(flag string) PreflightHook {
	spl := strings.Split(flag, "=")
	return preflightHooks[spl[0]]
}
