// service-launcher holds needed things to recognize and launch services
package main

import (
	"context"
	"fmt"
	"init/fs"
	"init/services"
	"init/socket"
	"io"
	"log"
	"os"
	"os/signal"
	"syscall"
)

var (
	logger = log.New(os.Stdout, "service-launcher ", log.LstdFlags)
)

func main() {
	// Skip boot allows callers to avoid boot tasks like mounting filesystems.
	// This setting can be useful in container environments where the setup is already done.
	skipBoot := len(os.Args) > 1 && os.Args[1] == "skipBoot"

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	sctx, scancel := context.WithCancel(ctx)
	deadChildrenChannel := make(chan bool, 5)
	go watchSignals(sctx, scancel, deadChildrenChannel)

	if !skipBoot {
		logger.Println("Mounting filesystems")
		mounter := fs.NewMounter()
		defer mounter.UmountAll()

		if err := mounter.MountAll(); err != nil {
			logger.Panicln("Mounter error: " + err.Error())
		}

		logger.Println("Setting hostname and keymap")
		if err := setHostname(); err != nil {
			logger.Panicln(err.Error())
		}

		if err := setKeymap(); err != nil {
			logger.Panicln(err.Error())
		}
	} else {
		logger.Println("Boot tasks were skipped")
	}

	logger.Println("Starting all services...")
	svc, err := services.NewServiceManager(sctx, deadChildrenChannel)
	if err != nil {
		logger.Panicln(err.Error())
	}

	if err = svc.StartAll(sctx); err != nil {
		logger.Panicln(err.Error())
	}
	defer svc.StopAll(ctx)

	commands := map[string]socket.CommandHandler{
		// Power managament
		"reboot":   handleReboot,
		"poweroff": handleReboot,
		"kexec":    handleReboot,

		// Manage services
		"list":    svc.HandleListCommand,
		"start":   svc.HandleServiceCommand,
		"stop":    svc.HandleServiceCommand,
		"restart": svc.HandleServiceCommand,
		"status":  svc.HandleServiceCommand,
		"dump":    svc.HandleServiceCommand,

		"reload": svc.HandleReloadCommand,

		// Get journal entries
		"journal":   svc.HandleJournalCommands,
		"journal-f": svc.HandleJournalCommands,
	}

	sckt, err := socket.NewListener(commands)
	if err != nil {
		panic(err)
	}
	defer sckt.Close()

	go sckt.Listen(sctx)

	<-sctx.Done()

	// Deferred functions will do the teardown
	logger.Println("Exiting service manager")
}

func watchSignals(ctx context.Context, cancel context.CancelFunc, deadChild chan<- bool) {
	defer cancel()
	defer close(deadChild)

	ch := make(chan os.Signal, 1)
	defer close(ch)
	signal.Notify(ch, syscall.SIGTERM, syscall.SIGINT, syscall.SIGCHLD)

	defer signal.Stop(ch)

	for {
		select {
		case <-ctx.Done():
		case sig := <-ch:
			switch sig {
			case syscall.SIGCHLD:
				// Dead body reported
				deadChild <- true

				continue // Nothing to see here
			default:
			}
		}

		break
	}
}

func handleReboot(command, _ string, respond io.WriteCloser) {
	defer respond.Close()

	var signal os.Signal

	switch command {
	case "reboot":
		signal = syscall.SIGINT
	case "poweroff":
		signal = syscall.SIGUSR1
	case "kexec":
		signal = syscall.SIGUSR2
	default:
		_, _ = respond.Write([]byte("Command " + command + " does not correspond with any signal"))
	}

	if signal == nil {
		return
	}

	fmt.Fprintf(respond, "Killing init with signal %s\n", signal.String())
	// PID 1 must exist
	process, err := os.FindProcess(1)
	if err != nil {
		fmt.Fprintf(respond, "Error finding parent: %s\n", err)
	}

	if err = process.Signal(signal); err != nil {
		fmt.Fprintf(respond, "Cannot kill parent (%d) with signal %s: %s\n", process.Pid, signal.String(), err)
	} else {
		fmt.Fprintf(respond, "Done\n")
	}
}
