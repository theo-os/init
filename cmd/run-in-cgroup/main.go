package main

import (
	"context"
	"flag"
	"os"
	"os/signal"

	"golang.org/x/sys/unix"
)

func main() {
	os.Exit(start())
}

func start() int {
	// Orphaned children should be reparented to us so we can wait(2) them.
	// Arguments 3-5 are useless for this call.
	err := unix.Prctl(unix.PR_SET_CHILD_SUBREAPER, 1, 0, 0, 0)
	if err != nil {
		panic(err)
	}

	// Config vars
	var (
		cgroupName, cgroupMount string
		arguments, cmdline      = splitArguments(os.Args[1:])
	)

	if len(cmdline) == 0 {
		panic("Must include -- and then the command to launch in a cgroup")
	}

	set := flag.NewFlagSet(os.Args[0], flag.ExitOnError)
	set.StringVar(&cgroupName, "name", "", "Path to cgroup (will panic if already exists). Mandatory")
	set.StringVar(&cgroupMount, "mount", "/sys/fs/cgroup", "Dir where cgroupfs is mounted")

	if err := set.Parse(arguments); err != nil {
		panic(err)
	}

	if cgroupName == "" {
		panic("Cgroup name is mandatory")
	}

	chld := make(chan bool, 2)
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	go watchSignals(ctx, cancel, chld)

	cgroup, err := NewCgroup(cgroupMount, cgroupName)
	if err != nil {
		panic(err)
	}
	defer cgroup.Teardown()

	if err = cgroup.Run(cmdline); err != nil {
		panic(err)
	}

	chld <- true // Trigger an immediate check

	return cgroup.WaitLoop(ctx, chld)
}

func watchSignals(ctx context.Context, cancel context.CancelFunc, sigchld chan<- bool) {
	defer cancel()
	ch := make(chan os.Signal, 1)
	signal.Notify(ch, unix.SIGINT, unix.SIGTERM, unix.SIGCHLD)

loop:
	for {
		select {
		case <-ctx.Done():
		case sig := <-ch:
			if sig == unix.SIGCHLD {
				select {
				case sigchld <- true:
				default:
				}
				continue
			}
		}
		break loop
	}
}

// Split arguments by "--"
func splitArguments(args []string) (args1, args2 []string) {
	sliceLength := len(args) / 2
	args1, args2 = make([]string, 0, sliceLength), make([]string, 0, sliceLength)
	receiver := &args1

	for _, arg := range args {
		if arg == "--" {
			receiver = &args2
			continue
		}
		*receiver = append(*receiver, arg)
	}

	return
}
