// Init only does required things like catching singals
package main

import (
	"context"
	"init/util"
	"log"
	"os"
	"os/signal"
	"syscall"
)

// Service Launcher will be restarted until the counter reaches this number. Then system will be halted.
const RestartSL = 10

var (
	logger = log.New(os.Stdout, "init ", log.LstdFlags)
)

func main() {
	var slArgs []string

	for _, arg := range os.Args {
		if arg == "skipBoot" {
			slArgs = append(slArgs, arg)
		}
	}

	if os.Getpid() != 1 {
		logger.Panicln("Program must be run as PID 1")
		return
	}
	logger.Println("GP1 -> Initializing")

	var rebootCommand, restartedTimes int

	for {
		process, err := launchServiceDaemon(slArgs)
		if err != nil {
			logger.Panicln(err.Error())
			return
		}

		ctx, cancel := context.WithCancel(context.Background())

		// Cancel context on Service Daemon exit
		go func() {
			defer cancel()
			if _, err := process.Wait(); err != nil {
				logger.Printf("Service Daemon stopped with error %s\n", err)
			}
		}()

		syscall.Reboot(syscall.LINUX_REBOOT_CMD_CAD_OFF)

		// Do the loop before we watch signals (sometimes there are dead processes before we launch anything, thanks initcpio)
		util.WaitpidLoop()
		rebootCommand = catchSignals(ctx, cancel)

		// Notify service manager
		process.Signal(syscall.SIGINT)
		// Enable CAD to be handled by kernel (now we're not watching signals)
		syscall.Reboot(syscall.LINUX_REBOOT_CMD_CAD_ON)

		logger.Println("Waiting for child process")
		<-ctx.Done()

		if rebootCommand == 0 {
			if restartedTimes < RestartSL {
				logger.Println("No reboot command from watcher. Starting service launcher again")
				restartedTimes++
				continue
			}

			logger.Println("Restarted times is over treshold. System will be halted")
			rebootCommand = syscall.LINUX_REBOOT_CMD_HALT
		}

		break
	}

	// If boot was skipped, reboot is not expected
	if len(slArgs) > 0 && slArgs[0] == "skipBoot" {
		return
	}

	// Uh-oh, we're exiting
	logger.Println("Syncing disks")
	syscall.Sync()
	logger.Println("Goodbye")
	if err := syscall.Reboot(rebootCommand); err != nil {
		panic(err)
	}
}

func catchSignals(ctx context.Context, cancel context.CancelFunc) (reboot int) {
	defer cancel()
	sigch := make(chan os.Signal, 4)
	defer close(sigch)
	signal.Notify(sigch, syscall.SIGUSR1, syscall.SIGUSR2, syscall.SIGCHLD, syscall.SIGINT, syscall.SIGTERM)
	defer signal.Stop(sigch)
	for reboot == 0 {
		select {
		case sig := <-sigch:
			switch sig {
			case syscall.SIGUSR1:
				reboot = syscall.LINUX_REBOOT_CMD_POWER_OFF
			case syscall.SIGUSR2:
				reboot = syscall.LINUX_REBOOT_CMD_KEXEC
			case syscall.SIGINT, syscall.SIGTERM:
				reboot = syscall.LINUX_REBOOT_CMD_RESTART
			case syscall.SIGCHLD:
				util.WaitpidLoop()
			}
		case <-ctx.Done():
			return
		}
	}

	return
}
