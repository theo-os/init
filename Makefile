OUTDIR:=out
DESTDIR:=/usr/local

sources=$(wildcard cmd/*)
dests=$(foreach thing,$(sources),$(OUTDIR)/$(notdir $(thing)))

.PHONY: all
all: $(OUTDIR) $(dests)

.PHONY: install
install:
	install -Dm 0711 $(dests) "$(DESTDIR)/bin/"

$(OUTDIR)/%: $(wildcard */*.go) $(wildcard cmd/*/*.go)
	go build -v -trimpath -ldflags '-s -w -extldflags=-static' -tags netgo -o "$(OUTDIR)/$(notdir $@)" "./cmd/$(notdir $@)"

$(OUTDIR):
	mkdir -p $(OUTDIR)

.PHONY: clean
clean:
	rm -rf out
