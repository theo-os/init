module init

go 1.18

require (
	github.com/fsnotify/fsnotify v1.4.9
	golang.org/x/sync v0.0.0-20201207232520-09787c993a3a
	golang.org/x/sys v0.0.0-20201223074533-0d417f636930
	gopkg.in/yaml.v3 v3.0.0-20200615113413-eeeca48fe776
)
